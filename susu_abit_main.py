# -*- coding: utf-8 -*-

import connect
import csv
import datetime
import susu_abit_parser
import threading
import time
import urls_of_directions

from queue import Queue


class Parser:
    def __init__(self):
        self.url = 'http://abit.susu.ru/rating/2016/rat_o_bs.php'
        self.susu_data = []
        self.threads_quantity = 4
        self.queue = Queue()

    def threader(self):
        while True:
            url = self.queue.get()
            data = susu_abit_parser.get_dict('http://abit.susu.ru' + url)
            if data:
                self.susu_data.append(dict({'url': url, 'data': data}))
            self.queue.task_done()

    def get_abits(self):
        main_page = connect.get_soup(self.url)
        urls_from_page = []

        for link in main_page.find_all('a'):
            urls_from_page.append(link.get('href'))

        regex = '(/rating/2016/list.+)*'
        urls = urls_of_directions.get_urls(urls_from_page, regex)

        for x in range(self.threads_quantity):
            t = threading.Thread(target=self.threader)
            t.daemon = True
            t.start()
        for url in urls:
            self.queue.put(url)

        self.queue.join()

        table_main_page = susu_abit_parser.get_dict(self.url)
        abits = []

        for item in self.susu_data:
            for row in table_main_page:
                [item.update({'faculty': row['Институт/Филиал'],
                              'direction': row['Направление/Специальность']})
                 if item['url'] in row[key] else None for key in row]
            [abit.update({'Институт/Филиал': item['direction'],
                          'Направление/Специальность': item['faculty']}) for abit in item['data']]
            abits.extend(item['data'])
        return abits

    @staticmethod
    def load_csv_file():
        data = []
        with open('2016-07-11 19-59.csv') as csvfile:
            reader = csv.DictReader(csvfile)
            for row in reader:
                data.append(row)
        return data

    @staticmethod
    def save_csv_file(abits):
        keys = []
        for abit in abits:
            [keys.append(key) if key not in keys else None for key in abit.keys()]

        for abit in abits:
            [abit.update({key: '-'}) if key not in abit.keys() else None for key in keys]

        print(keys)
        filename = datetime.datetime.now().strftime("%Y-%m-%d %H-%M.csv")
        with open(filename, 'w') as output_file:
            dict_writer = csv.DictWriter(output_file, keys)
            dict_writer.writeheader()
            dict_writer.writerows(abits)


def main():
    start = time.time()
    print('In process...')

    parser = Parser()
    abits = parser.get_abits()
    print(abits[0])

    print('Entire job took:', time.time() - start)
    print("done")

if __name__ == "__main__":
    main()
