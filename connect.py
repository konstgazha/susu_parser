# -*- coding: utf-8 -*-
"""
Created on Wed Jul  6 16:32:32 2016

@author: Konstantin
"""

import random

from bs4 import BeautifulSoup
from fake_useragent import UserAgent
from urllib.request import Request, urlopen
from time import sleep


def get_soup(url):
    sleep((random.randint(5, 10)) * 0.1)
    request = Request(url, headers={'User-Agent': UserAgent().random})
    response = urlopen(request)
    soup = BeautifulSoup(response.read().decode('utf-8'), 'html5lib')
    return soup
