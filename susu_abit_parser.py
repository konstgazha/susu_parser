import connect
import re


def get_dict(url):
    abits = connect.get_soup(url)
    abits = abits.find_all('tr')
    data = []
    if abits:
        title = get_data_row(abits[0].find_all('th'))
    for abit in abits:
        abit = abit.find_all('td')
        row = get_data_row(abit)
        if row:
            row_dic = dict()
            for i in range(0, len(row)):
                try:
                    row_dic.update({title[i]:row[i]})
                except:
                    row_dic.pop(title[-1])
                    break
            data.append(row_dic)
    return data


def get_data_row(abit):
    row = []
    for i in range(0, len(abit)):
        result = re.search('>(.+?)<', str(abit[i]))
        if result:
            row.append(result.group(1))
        else:
            row.append('-')
    return row
