# -*- coding: utf-8 -*-
"""
Created on Mon Jul 18 19:46:01 2016

@author: Konstantin
"""

import re


def get_urls(mas_of_urls, regex):
    cleaned_mas = []
    for url in mas_of_urls:
        cleaned = re.search(regex, str(url))
        if cleaned:
            if cleaned.group(0):
                cleaned_mas.append(cleaned.group(0))
    return cleaned_mas
